# SPDX-FileCopyrightText: © 2020 Carmen Bianca Bakker <carmen@carmenbianca.eu>
#
# SPDX-License-Identifier: Apache-2.0

import json
import os

import requests


def main():
    auth = (os.environ.get("GITHUB_USER"), os.environ.get("GITHUB_AUTH"))
    if not all(auth):
        auth = None

    url = "https://api.github.com/repos/revuloj/revo-fonto/releases"
    response = requests.get(url, auth=auth)
    content = json.loads(response.text)

    # Assets of newest release
    assets = content[0]["assets"]
    for asset in assets:
        if (name := asset.get("name")) :
            if not name.startswith("revoart"):
                continue
            else:
                download_url = asset["browser_download_url"]

    revoart = requests.get(download_url, auth=auth)
    with open("revoart.zip", "wb") as fp:
        fp.write(revoart.content)


if __name__ == "__main__":
    main()
