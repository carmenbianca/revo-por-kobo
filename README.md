<!--
SPDX-FileCopyrightText: © 2020 Carmen Bianca Bakker <carmen@carmenbianca.eu>

SPDX-License-Identifier: Apache-2.0
-->

# ReVo por Kobo

> Ilo por krei vortaron uzeblan sur Kobo-legilo.

Oficiale por Kobo ne disponeblas Esperanta vortaro. Ĉi tiu ilo disponebligas la
[Retan Vortaron](http://www.reta-vortaro.de/revo/) je la proprieta(?)
dosierformo de Kobo.

## Elŝuti la vortaron

Por elŝuti la vortaron, iru al
<https://gitlab.com/carmenbianca/revo-por-kobo/-/pipelines> kaj elŝutu la plej
freŝdatan kobo:archive-artefakton. Ĉiusemajne disponeblos nova versio.

![GitLab GUI](gitlab-gui.png)

Ene de la artifacts.zip-dosiero, vi trovos dicthtml-eo.zip. Ne necesas elpaki
tiun zip-dosieron.

## Sendi la vortaron al la Kobo-legilo

Konektu la Kobo-legilon al via komputilo. Surmetu la dosiersistemon. Kopiu
dicthtml-eo.zip al `.kobo/dict/dicthtml-$LANG.zip`. Ĉar ne konas la lingvokodon
`eo` la Kobo-legilo, nepras ke vi anstataŭigu `$LANG` per alia lingvokodo, kiun
ja konas la Kobo-legilo: Ekzemple `it`, `fr`, `de`, `nl`, ktp.

## Uzi la vortaron

Post ol malkonekti vian Kobo-legilon, vi povos uzi la vortaron. Notu, ke la
vortaro indeksiĝas per vort-radikoj. Ekzemple, por trovi la difinon de
"ŝanĝiĝi", elektu nur "ŝanĝ" sur via legilo.

Se vi volas permane tajpi vorton sur la Kobo-legilo, uzu la x-sistemon. "ŝanĝ"
kaj "sxangx" do havas la samajn difinojn.

## Permane krei la vortaron

Por altnivelaj uzantoj, trovu sube la komandojn por permane krei la vortaron:

```bash
$ pip install --user requests penelope beautifulsoup4 click
$ python download_latest_revo.py
$ unzip revoart.zip
$ python art_al_xml.py revo/art/*.html -o out.xml
$ penelope -i out.xml -j xml -f eo -t eo -p kobo -o dicthtml-eo.zip
```

## Kiel funkcias?

Disponeblas krudaj HTML-dosieroj ene de `revoart.zip`. Ĉiu dosiero estas unuopa
radiko. Ĉi tiu ilo eltiras el ĉiu dosiero la nomon de la radiko kaj la subajn
difinojn, kaj metas tiujn informojn en XML-dosieron.

Ekzemple:

```xml
<dictionary>
  <entry>
    <key>banan</key>
    <def>Ekzempla teksto</def>
  </entry>
  <entry>
    [...]
  </entry>
</dictionary>
```

La ekzempla teksto en la `def`-kampo estas "eskapigita" HTML. Alie ne funkcias
la konvertado de Penelope.

Penelope estas ilo kiu povas konverti la supran XML-formon al Kobo-dosierformo.

Jen vortaro!

## Dankoj

Dankoj al
[marko31/konvertado-de-revo](https://bitbucket.org/marko31/konvertado-de-revo/)
kaj [pettarin/penelope](https://github.com/pettarin/penelope).

## Permesilo

Ĉi tiu ilo estas libera programaro sub la permesilo Apache-2.0. ReVo estas
libera sub la permesilo GPL-2.0.
