# SPDX-FileCopyrightText: © 2020 Carmen Bianca Bakker <carmen@carmenbianca.eu>
#
# SPDX-License-Identifier: Apache-2.0

from html import escape
from multiprocessing.pool import Pool
import sys

import click
from bs4 import BeautifulSoup


def reduced_title(title: str) -> str:
    return title.split("/")[0]


def x_system_title(title: str) -> str:
    letters = {
        "ĉ": "cx",
        "ĝ": "gx",
        "ĥ": "hx",
        "ĵ": "jx",
        "ŝ": "sx",
        "ŭ": "ux",
        "Ĉ": "CX",
        "Ĝ": "GX",
        "Ĥ": "HX",
        "Ĵ": "JX",
        "Ŝ": "SX",
        "Ŭ": "UX",
    }

    for key, val in letters.items():
        title = title.replace(key, val)

    return title


def all_function(filename: str) -> str:
    with open(filename) as fp:
        content = fp.read()
    soup = BeautifulSoup(content, "html.parser")

    title = reduced_title(soup.title.string)
    x_title = x_system_title(title)

    article = soup.article
    definition = escape(str(article))

    out = f"<entry><key>{title}</key><def>{definition}</def></entry>"
    if title != x_title:
        out += f"<entry><key>{x_title}</key><def>{definition}</def></entry>"
    return out


@click.command()
@click.argument("article", nargs=-1)
@click.option("-o", "--output", type=click.File("w"))
def main(article, output):
    output.write("<dictionary>\n")
    pool = Pool()
    results = [pool.apply_async(all_function, (filename,)) for filename in article]

    for result in results:
        output.write(result.get())

    output.write("</dictionary>")


if __name__ == "__main__":
    main()
